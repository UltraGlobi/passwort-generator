<?php
/* *******************************************
 * Author:			Silvan Pfister
 * Project:			Passwortgenerator
 * Version:			1.1
 * Creation-Date:	24.05.2017
 * Licence:			MIT
 *
 *				Description:
 * Index des Passwortgenerators. Da dieses
 * Projekt keine Anforderungen hat welche
 * das anschliessen des Projektes an eine
 * andere Webseite vorsieht, ist der Generator
 * als eigenständige Seite aufgebaut.
 ********************************************/
include_once("php/Session.php");
?>
<!DOCTYPE html>
<html>

<head>
	<title>Passwortgenerator</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="js/libs/jquery.min.js" type="text/javascript"></script>
	<script src="js/libs/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/libs/seedrandom.js" type="text/javascript"></script>
	<script src="js/libs/sha256.js" type="text/javascript"></script>
	<script src="js/functions.js" type="text/javascript"></script>
	<script src="js/KeyGen.js" type="text/javascript"></script>
</head>

<body>
	<div class="container">
		<div class="jumbotron">
			<div class="row">
				<!-- 
                        Das Banner mitsamt Titel kommt in einen eigenen Wrapper.
                        Das Banner ist momentan noch mit einem Platzhalter versehen
                    -->
				<div id="Banner" class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
					<img src="img/banner.bmp" alt="Passwortgenerator" class="img-responsive" />
				</div>
				<!-- 
                        Das Login-Menu wird in einer Separaten Datei generiert.
                        Dort wird auch überprüft ob der Benutzer eingeloggt ist
                        und in dem Fall ein Logout-Button angezeigt.
                    -->
				<div id="Menu" class="col-lg-3 col-lg-offset-1
                         col-md-4 col-md-offset-2
                         col-sm-5 col-sm-offset-1
                         col-xs-12 col-xs-offset-0">
					<?php include "php/menu.php" ?>
				</div>
			</div>
		</div>
		<!--
                Falls der User kein Javascript aktiviert hat, wird er 
                dazu aufgefordert, da es ohne JS keine Funktionalität gibt.
            -->
		<noscript>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-danger">
						<div class="panel-heading">
							<h1 class="panel-title">Kein JavaScript aktiv!</h1>
						</div>
						<div class="panel-body">
							Ohne JavaScript kann diese Webseite nicht funktionieren.
							Aktivieren Sie daher ihr JavaScript in ihren Browsereinstellungen. <br>
							Die Anleitung dazu finden Sie auf <a href="http://www.enable-javascript.com/">Enable Javascript.com</a>.
						</div>
					</div>
				</div>
			</div>
		</noscript>
		<!-- 
                Hier kommt nun das eigentliche Passwort-Formular.
                Es wird kein Datenverkehr stattfinden da es sich ja um
                ein vertrauliches Passwort handelt. Alle eingaben
                werden daher mit JavaScript gehandhabt.
            -->
		<div class="well">
			<form id="KeyGen">
				<div class="input-group">
					<span class="input-group-addon" id="seed-desc">Seed</span>
					<input type="text" class="form-control" id="Seed" aria-describedby="seed-desc" placeholder="z.B. 'Google Account' oder 'Beispiel-AG' (Optional)" oninput="updatePass(this)">
				</div>
				<div class="row">
					<!-- ____________________________ Checkbox Kleinbuchstaben ____________________________ -->
					<div class="col-lg-3 col-md-3 col-sm-4">
						<div class="input-group charpool" data-toggle="tooltip" data-placement="top" title="a-z">
							<span class="input-group-addon">
								<input onclick="updatePass(this)" type="checkbox" aria-label="Kleinbuchstaben" aria-describedby="chars-low" id="LowCase">
							</span>
							<input readonly onclick="toggleCheck(this)" class="form-control unselectable" id="chars-low" value="Kleinbuchstabe">
						</div> <!-- /input-group -->
					</div> <!-- /col -->
					<!-- ____________________________ Checkbox Grossbuchstaben ____________________________ -->
					<div class="col-lg-3 col-md-3 col-sm-4">
						<div class="input-group charpool" data-toggle="tooltip" data-placement="top" title="A-Z">
							<span class="input-group-addon">
								<input onclick="updatePass(this)" type="checkbox" aria-label="Grossbuchstaben" aria-describedby="chars-up" id="UpCase">
							</span>
							<input readonly onclick="toggleCheck(this)" class="form-control unselectable" id="chars-up" value="Grossbuchstabe">
						</div> <!-- /input-group -->
					</div> <!-- /col -->
					<!-- ____________________________ Checkbox Zahlen ____________________________ -->
					<div class="col-lg-3 col-md-3 col-sm-4">
						<div class="input-group charpool" data-toggle="tooltip" data-placement="top" title="0-9">
							<span class="input-group-addon">
								<input onclick="updatePass(this)" type="checkbox" aria-label="Zahlen" aria-describedby="chars-num" id="Numbers">
							</span>
							<input readonly onclick="toggleCheck(this)" class="form-control unselectable" id="chars-num" value="Zahl">
						</div> <!-- /input-group -->
					</div> <!-- /col -->
					<!-- ____________________________ Checkbox Umlaute ____________________________ -->
					<div class="col-lg-3 col-md-3 col-sm-4">
						<div class="input-group charpool" data-toggle="tooltip" data-placement="top" title="äÄöÖüÜèé...">
							<span class="input-group-addon">
								<input onclick="updatePass(this)" type="checkbox" aria-label="Umlaute" aria-describedby="chars-uml" id="Umlaute">
							</span>
							<input readonly onclick="toggleCheck(this)" class="form-control unselectable" id="chars-uml" value="Umlaut">
						</div> <!-- /input-group -->
					</div> <!-- /col -->
					<!-- ____________________________ Checkbox Symbole ____________________________ -->
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="input-group charpool" data-toggle="tooltip" data-placement="top" title="+-=?!:;...">
							<span class="input-group-addon">
								<input onclick="updatePass(this)" type="checkbox" aria-label="Symbole" aria-describedby="chars-sym" id="Symbols">
							</span>
							<input readonly onclick="toggleCheck(this)" class="form-control unselectable" id="chars-sym" value="Symbol">
						</div> <!-- /input-group -->
					</div> <!-- /col -->
					<!-- ____________________________ Checkbox Komplexe Symbole ____________________________ -->
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="input-group charpool" data-toggle="tooltip" data-placement="top" title="[]{}()&lt;&gt;&amp;^$...">
							<span class="input-group-addon">
								<input onclick="updatePass(this)" type="checkbox" aria-label="Komplexe Symbole" aria-describedby="chars-cmsym" id="Com-Symbols">
							</span>
							<input readonly onclick="toggleCheck(this)" class="form-control unselectable" id="chars-cmsym" value="Komplexe Symbole">
						</div> <!-- /input-group -->
					</div> <!-- /col -->
					<!-- ____________________________ Checkbox Sehr Komplexe Symbole ____________________________ -->
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="input-group charpool" data-toggle="tooltip" data-placement="top" title="ƒ∏∐∑∓∔...">
							<span class="input-group-addon">
								<input onclick="updatePass(this)" type="checkbox" aria-label="Sehr Komplexe Symbole" aria-describedby="chars-vcmsym" id="V-Com-Symbols">
							</span>
							<input readonly onclick="toggleCheck(this)" class="form-control unselectable" id="chars-vcmsym" value="Sehr Komplexe Symbole">
						</div> <!-- /input-group -->
					</div> <!-- /col -->
					<!-- ____________________________ Checkbox & Eingabe Eigene Charaktere ____________________________ -->
					<div class="col-md-12 col-sm-8">
						<div class="input-group charpool">
							<span class="input-group-addon">
								<input onclick="updatePass(this)" type="checkbox" aria-label="Eigene Charaktere" aria-describedby="chars-custom" id="Custom_Chars">
							</span>
							<input oninput="updatePass(this)" class="form-control" id="chars-custom" placeholder="Eigene Charaktere">
						</div> <!-- /input-group -->
					</div> <!-- /col -->
				</div> <!-- /row -->
				<div class="row">
					<!-- ____________________________ Passwortlänge ____________________________ -->
					<div class="col-sm-6">
						<div class="input-group numchars">
							<span class="input-group-addon" id="num-desc">Passwortl&auml;nge</span>
							<input type="number" class="form-control" placeholder="Die L&auml;nge des Passwortes (1-1024)" id="Len" aria-describedby="num-desc" oninput="updatePass(this)" max="1024" min="1" value="1">
						</div> <!-- /input-group -->
					</div> <!-- /col -->
					<!-- ____________________________ Passwort Ausgabe ____________________________ -->
					<div class="col-sm-6">
						<div class="input-group">
							<span class="input-group-addon" id="pass-desc">Passwort</span>
							<input readonly type="text" class="form-control" placeholder="Hier erscheint Ihr Passwort" id="Pass" aria-describedby="pass-desc">
						</div> <!-- /input-group -->
					</div> <!-- /col -->
				</div> <!-- /row -->
			</form> <!-- /keygen -->
		</div> <!-- /well -->
		<div class="well">
			<div class="row">
				<div class="col-sm-6">
					<div class="input-group">
						<span class="input-group-addon" id="passex-desc">Passwort</span>
						<input type="text" class="form-control" oninput="updateComplexity(this,'custom_complex')" placeholder="Geben Sie ein Passwort zum &uuml;berpr&uuml;fen ein" id="Passex" aria-describedby="passex-desc">
					</div> <!-- /input-group -->
				</div> <!-- /col -->
				<div class="col-sm-6">
					<div class="progress">
						<div id="custom_complex" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">Kein Passwort</div>
					</div>
				</div> <!-- /col -->
			</div> <!-- /row -->
		</div> <!-- /well -->
		<div>
			<!-- Footer -->
			<p>
				<a href="https://gitlab.com/UltraGlobi/passwort-generator">Passwortgenerator - &copy; Silvan Pfister 2017</a>
				<br>
				External Libraries used for this project:
				<br>
				<a href="https://github.com/emn178/js-sha256">Hash Function for Password Encryption - Copyright (c) 2014-2017 Chen, Yi-Cyuan</a>
				<br>
				<a href="https://github.com/davidbau/seedrandom">Seeded Random for Password Generator - Copyright 2014 David Bau</a>
				<br>
			</p>
		</div>
	</div>
	<?php
	// Includes a set of Modal forms
	include "php/modals.php";
	?>
	<script type="text/javascript">
		getHash(function(result) {
			KeyData.MasterHash = result;
			reset();
		});
		$(function() {
			$('[data-toggle="tooltip"]').tooltip()
		});
	</script>
</body>

</html>