<?php
/* *******************************************
 * Author:			Silvan Pfister
 * Project:			Passwortgenerator
 * Version:			1.1
 * Creation-Date:	29.05.2017
 * Licence:			MIT
 *
 *				Description:
 * Dies ist das Login/out - Menu. Hier wird
 * die Datenbankverbindung hergestellt und
 * die Daten werden geladen.
 ********************************************/
 
include_once "php/Session.php";

// ***************************************** Start Menu construction

if (isset($_POST['logOut'])){ // User is logging out
	LogOut();
} else if (User::$LoggedIn) { // User is logged in
	logoutMenu();
} else if(isset($_POST['register'])){ // User is registering an account
	Register();
} else {
	if (isset($_POST['logIn'])){ // User is logging in
		if(isNameTaken($_POST['name'])){ // Existing account
			if (checkPass($_POST['name'], $_POST['pass'])){ // Login Success
				LogIn();
			} else { // Wrong Password
				echo "Falsches Passwort!<br>";
				loginMenu($_POST['name']);
			}
		} else { // New Account / Attempt to register
			registrationMenu($_POST['name'], $_POST['pass']);
		}
	} else { // User is simply visiting the site
		loginMenu();
	}
} // end else .. elseif loggedIn .. elseif isset logout .. if isset register

// ***************************************** End Menu construction

/**
 * Prints a log-in Menu
 * @param String $name The name of the User
 * @return void
 */
function loginMenu($name = ""){
	echo
	"<form name='logIn' method='POST' autocomplete='off'>
		<div class='input-group'>
			<span class='input-group-addon' id='uname-desc'>Name</span>
			<input name='name' autocomplete='off' type='text' class='form-control' aria-describedby='uname-desc'
			placeholder='Benutzername' value='$name'>
		</div>
		<div class='input-group'>
			<span class='input-group-addon' id='upass-desc'>Passwort</span>
			<input autocomplete='off' type='password' class='form-control' aria-describedby='upass-desc'
			placeholder='Passwort' id='unenc_pass'>
		</div>
		<input name='pass' type='hidden' id='enc_pass'>
		<input type='hidden' name='logIn' value='true'>
	</form>
	<button type='button' class='btn btn-default' onclick='submitEncrypted()' >LogIn</button>";
}

/**
 * Prints a password confirmation
 * @param String $name The name of the user
 * @param String $pass The encrypted password of the user
 * @return void
 */
function registrationMenu($name, $pass){
	echo 
	"Neuen Benutzer anlegen<br>
	<form name='register' method='POST' autocomplete='off'>
		<div class='input-group'>
			<span class='input-group-addon' id='upass-desc'>Passwort best&auml;tigen</span>
			<input style='display:none' type='password'>
			<input autocomplete='off' type='password' class='form-control' aria-describedby='upass-desc'
			placeholder='Passwort' id='unenc_pass'>
		</div>
		<input name='pass' type='hidden' id='enc_pass'>
		<input name='name' type='hidden' value='$name'>
		<input name='pass_enc' type='hidden' value='$pass'>
		<input type='hidden' name='register' value='true'>
	</form>
	<button type='button' class='btn btn-default'  onclick='submitEncrypted()'>Registrieren</button>";
}

/**
 * Prints the username and a log-out Menu
 * @return void
 */
function logoutMenu(){
	$uname = User::$Username;
	echo 
	"<div>Eingeloggt als $uname</div>
	<form method='POST' autocomplete='off'>
		<input class='btn btn-default'  type='submit' value='LogOut' >
		<input type='hidden' name='logOut' value='true'>
	</form>";
}

/**
 * Checks if an username is available for use.
 * @var String $name The username to check if taken.
 * @return bool True if the name is taken, false if available.
 */
function isNameTaken($name){
	$conn = Connect();
	$uname = $conn->escape_string($name);
	$taken = $conn->real_query(
				"SELECT * FROM user
				WHERE name = '$uname'") &&
			count($conn->store_result()->fetch_all()) > 0;
	$conn->close();
	return $taken;
}

/**
 * Checks if the password for the specified user is correct.
 * @param String $name The name of the user
 * @param String $pass The encrypted password of the user
 * @return bool True if the password matches the user, otherwise false
 */
function checkPass($name, $pass){
	$conn = Connect();
	$uname = $conn->escape_string($name);
	$upass = $conn->escape_string($pass);
	$matches = $conn->real_query(
				"SELECT * FROM user
				WHERE name = '$uname'
				AND password = '$upass'") &&
			count($conn->store_result()->fetch_all()) > 0;
	$conn->close();
	return $matches;
}
/**
 * Logs the user out and prints the login-menu via the loginMenu() method
 * @return void
 */
function LogOut(){
	session_unset(); 
	// Session state changed, reinitialize
	User::$initiated = FALSE;
	User::init();
	loginMenu();
}

/**
 * Logs the user in and prints the logout-menu via the logoutMenu() method
 */
function LogIn(){
	$conn = Connect();
	$uname = $conn->escape_string($_POST['name']);
	$upass = $conn->escape_string($_POST['pass']);
	$conn->real_query(
				"SELECT * FROM user
				WHERE name = '$uname'
				AND password = '$upass'");
	$result = $conn->store_result()->fetch_all(MYSQLI_ASSOC);
	$conn->close();
	$_SESSION["loggedIn"] = true;
	$_SESSION["Name"] = $result[0]['name'];
	$_SESSION["Hash"] = $result[0]['masterhash'];
	// Session state changed, reinitialize
	User::$initiated = FALSE;
	User::init();
	logoutMenu();
}

/**
 * Registers the user, logs him in and prints the logount-menu via the logoutMenu() method.
 */
function Register(){
	$conn = Connect();
	$uname = $conn->escape_string($_POST['name']);
	$upass = $conn->escape_string($_POST['pass']);
	$uhash = hash("sha256", $uname);
	for ($i=0; $i < 64 ; $i++) {  $uhash[$i] = (int)$uhash[$i] + (int)$upass[$i]; }
	$uhash = hash("sha256", $uhash);
	$conn->query(
		"INSERT INTO user(name, password, masterhash)
		VALUES ('$uname', '$upass', '$uhash')");
	$conn->close();
	$_SESSION['loggedIn'] = true;
	$_SESSION['Name'] = $_POST['name'];
	$_SESSION['Hash'] = $uhash;
	// Session state changed, reinitialize
	User::$initiated = FALSE;
	User::init();

	logoutMenu();
}

function Connect(){
	// TODO: create config file to retrieve db login data
	return new mysqli("localhost", "root", "", "key_generator");
}