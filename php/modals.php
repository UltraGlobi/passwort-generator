<?php
/* *******************************************
 * Author:			Silvan Pfister
 * Project:			Passwortgenerator
 * Version:			1.1
 * Creation-Date:	29.05.2017
 * Licence:			MIT
 *
 *				Description:
 * Erstellt Bootstrap-Modal-Dialoge für die
 * Webseite.
 ********************************************/

create_Modal(
  "noCharsModal",
  "Keine Charaktere f&uuml;r das Passwort",
  "W&auml;hlen Sie mindestens eine Kategorie für die Passwortgenerierung aus.
		Wenn Sie ein benutzerdefiniertes Set an Charakteren haben m&ouml;chten,
		m&uuml;ssen sie diese im letzten rot markierten Feld eingeben und das H&auml;ckchen
		setzen.");

create_Modal(
  "charCountModal",
  "Die Anzahl der Charaktere ist ung&uuml;ltig",
  "W&auml;hlen Sie 1 - 1024 Zeichen f&uuml;r die L&auml;nge des Passwortes aus.
    Weniger als 1 Zeichen macht keinen Sinn. Mehr als 1024 sind auch unn&ouml;tig.");

/**
 * Creates a modal for use with bootstrap
 * @param String $Header The title of the modal popup
 * @param String $Content The content of the modal popup 
 */
function create_Modal($ID, $Title, $Content){
  echo "<div class='modal fade' id='$ID' tabindex='-1' role='dialog' aria-labelledby='".$ID."Label'>
    <div class='modal-dialog' role='document'>
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
          <h4 class='modal-title' id='".$ID."Label'>$Title</h4>
        </div>
        <div class='modal-body'>
          $Content
        </div>
        <div class='modal-footer'>
          <button type='button' class='btn btn-default' data-dismiss='modal'>Ok</button>
        </div>
      </div>
    </div>
  </div>";
}
?>