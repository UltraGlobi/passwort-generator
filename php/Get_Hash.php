<?php
/* *******************************************
 * Author:			Silvan Pfister
 * Project:			Passwortgenerator
 * Version:			1.1
 * Creation-Date:	24.05.2017
 * Licence:			MIT
 *
 *				Description:
 * Holt den Hash des registrierten Benutzers
 ********************************************/

include_once 'Session.php';
    
if (User::$LoggedIn) {
    echo User::$MasterHash;
} else {
    echo "__CANCEL";
}
?>