<?php
/* *******************************************
* Author:			Silvan Pfister
* Project:			Passwortgenerator
* Version:			1.1
* Creation-Date:	24.05.2017
* Licence:			MIT
*
*				Description:
* Ladet Variablen und initialisiert die session.
********************************************/

session_start();
/**
 * Enthält Daten der momentanen Session
 */
class User
{
    /** 
     * Wahr, falls schon initiiert
     * @var bool $initiated
     */
    static $initiated = false;
    /**
     * Ist der Benutzer eingeloggt?
     * @var bool $LoggedIn
     */
    static $LoggedIn;
    /**
     * Der Benutzername
     * @var string $Username
     */
    static $Username;
    /**
     * Der Hash des Benutzers
     * @var String $MasterHash
     */
    static $MasterHash;

    /** 
     * Initiiert die User-Klasse.
     * Manuelles Aufrufen ist unnötig, es passiert automatisch beim Includen.
     * @return void
     */
    static function init()
    {
        if (!User::$initiated){
            User::$initiated = true;
            User::$LoggedIn = isset($_SESSION["loggedIn"]);
            if (User::$LoggedIn) { //Handle known user
                User::$Username = $_SESSION["Name"];
                User::$MasterHash = $_SESSION["Hash"];
            }
        }
    }
}


User::init();