/* *******************************************
 * Author:			Silvan Pfister
 * Project:			Passwortgenerator
 * Version:			1.1
 * Creation-Date:	24.05.2017
 * Licence:			MIT
 *
 *				Description:
 * Dies ist die KeyData Klasse. Sie handhabt
 * die Generierung der Passwörter.
 ********************************************/


/**
 * Feel free to edit. However your passwords won't be correct anymore if you do.
 * @readonly
 * @private
 * @type {string}
 */
var Hash;

/**
 * @type {{MasterHash: string, Specs: KeySpecs, Password(ID:string):string}}
 */
var KeyData = {
	Specs: defaults.KeySpecs,
	/** 
	 * Generates a Password using the Specs, the given ID and the MasterHash if available.
	 * 
	 * Is there no MasterHash available, it will use a random value instead when no ID is given.
	 * 
	 * If an ID is given, the resulting password will be the same unless a MasterHash exists.
	 * @param {string} ID An identifier used when seeding the random value for the password.
	 * @returns {string} A Password defined by the Specs
	 */
	Password: function(ID){
		if (ID == null) ID = "";
		let pass = "";
		let CharPool = "";
		if (KeyData.Specs.Custom) CharPool += KeyData.Specs.CustomCharPool;
		if (KeyData.Specs.LowCase) CharPool += charPools.LowCase;
		if (KeyData.Specs.UpCase) CharPool += charPools.UpCase;
		if (KeyData.Specs.Numbers) CharPool += charPools.Numbers;
		if (KeyData.Specs.Umlaut) CharPool += charPools.Umlaut;
		if (KeyData.Specs.Sym) CharPool += charPools.Sym;
		if (KeyData.Specs.Complex_Sym) CharPool += charPools.Complex_Sym;
		if (KeyData.Specs.Very_Comp_Sym) CharPool += charPools.Very_Comp_Sym;

		/* KeyPool has been populated.
		 * now we remove duplicate entries. */
		let temp = "";
		for(let i = 0; i <= CharPool.length; i++){
			if (temp.indexOf(CharPool.charAt(i)) == -1)
				temp += CharPool.charAt(i);
		}
		CharPool = temp;

		/** @type {number} */
		let CharCount = CharPool.length;
		if (KeyData.MasterHash != "__CANCEL") Hash = KeyData.MasterHash;
		else {
			Math.seedrandom(Date.now());
			Hash = "" + Math.random();
		}

		/* The new Random generator does not work as intended. Repeating patterns
		 * cause the generation to repeat the same result somehow.
		 * to prevent this the following block is used to give each position a weighted value.
		*/
		var newID = "";
		for (var i = 0; i < ID.length; i++){
			newID += String.fromCharCode(ID.charCodeAt(i) * (i+1));
		}
		ID = newID;


		if (ID!="" && KeyData.MasterHash == "__CANCEL") Math.seedrandom(ID);
		else if (ID != "") Math.seedrandom(Hash + ID)
		else Math.seedrandom(Hash)
		for (var i = 0; i < KeyData.Specs.Length; i++) {
			let index = Math.floor(Math.random() * (CharCount));
			pass += CharPool.charAt(index);
		}
		return pass;
	}
};