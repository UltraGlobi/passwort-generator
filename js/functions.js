/* *******************************************
 * Author:			Silvan Pfister
 * Project:			Passwortgenerator
 * Version:			1.1
 * Creation-Date:	24.05.2017
 * Licence:			MIT
 *
 *				Description:
 * Auslagerung der Funktionen für die Webseite
 ********************************************/

//Typendefinitionen
/**
 * @typedef {function(string):void} hashCallback
 * @typedef {function(string):void} errorCallback 
 * @typedef KeySpecs
 * @prop {boolean} LowCase
 * @prop {boolean} UpCase
 * @prop {boolean} Numbers
 * @prop {boolean} Umlaut
 * @prop {boolean} Sym
 * @prop {boolean} Complex_Sym
 * @prop {boolean} Very_Comp_Sym
 * @prop {boolean} Custom
 * @prop {string} CustomCharPool
 * @prop {number} Length
 */

/**
 * Liste von Komplexitätswerten
 * @readonly
 * @enum {number}
 */
var complexity = Object.freeze({
	EMPTY: 0,
	LOW: 1,
	MEDIUM: 2,
	HIGH: 3
});

/**
 * Liste von ReadyStates für XMLHttpRequests
 * @readonly
 * @enum {number}
 */
var readystates = Object.freeze({
	/** Die Nachricht wurde noch nicht gesendet */
	UNSENT: 0,
	/** Die Nachricht wurde geöffnet */
	OPENED: 1,
	/** Der Nachrichtenheader ist angekommen */
	HEADERS_RECEIVED: 2,
	/** Die Nachricht wird geladen */
	LOADING: 3,
	/** Die Nachricht wurde abgeliefert */
	DONE: 4
});
/**
 * Liste von Stati für XMLHttpRequests.
 * @readonly
 * @enum {number}
 */
var stati = Object.freeze({
	/** Nicht bereit */
	NOT_READY: 0,
	/** Fertig */
	DONE: 200
});

var charPools = Object.freeze({
	LowCase: "abcdefghijklmnopqrstuvwxyz",
	UpCase: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
	Numbers: "0123456789",
	Sym: ".,-+*/=\\;:_?! ",
	Complex_Sym: "[]{}()<>£$¨&^`~´'¢|¬§°#@¦\"ç%&€",
	Very_Comp_Sym: "ƒ∏∐∑∓∔∠∿␦⛧⛜✐⛕⛏⚝☠☢☣☃௵⬤⬢⬛⬟", //@TODO: Generate Complex Symbol List
	Umlaut: "âáàãäêéèëîíìïôóòõöûúùüñýÿÂÁÀÃÄÊÉÈËÎÍÌÏÔÓÒÕÖÛÚÙÜÑÝŸ"
})

var defaults = Object.freeze({
	/** @type {KeySpecs} */
	KeySpecs: {
		Length: 8,
		LowCase: true,
		UpCase: true,
		Umlaut: false,
		Numbers: true,
		Sym: true,
		Complex_Sym: false,
		Very_Comp_Sym: false,
		Custom: false,
		CustomCharPool: "",
	}
});


function getHash(callback) { getHash(callback, null); }

/**
 * Erstellt ein XMLHttpRequest und versucht den Hash des momentanen users abzurufen.
 * Der 'failed' Parameter ist optional. Er wird als alternative zu Console.log angewandt
 * @param {hashCallback} callback - Bei Erfolg aufgerufen. Enthält den Hash im Parameter
 * @param {errorCallback} failed - Bei Fehler aufgerufen. Enthält die Fehlermeldung um Parameter
 */
function getHash(callback, failed) {
	try {
		let xhttp = new XMLHttpRequest();

		xhttp.open("POST", "php/Get_Hash.php", true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send();
		xhttp.onreadystatechange = function () {
			if (this.readyState == readystates.DONE && this.status == stati.DONE) {
				callback(this.responseText);
			}
		}
		xhttp.onabort = function () {
			failed("Message was aborted");
		}
		xhttp.ontimeout = function () {
			failed("Message Timed out");
		}
		xhttp.onerror = function (sender, ev) {
			failed(ev.error.message);
		}
	} catch (error) {
		if (failed != null) failed(error.message);
		else console.log(error.message);
	}
}

function updatePass() { updatePass(null); }
/**
 * Diese Methode generiert ein Passwort
 * und zeigt dieses an
 * 
 * Die Parameter werden komplett ignoriert...
 * @param {HTMLInputElement} sender
 */
function updatePass(sender) {
	let len = document.getElementById("Len").value;
	if (!isNaN(len) && (len = parseInt(len)) <= 1024 && len >= 1) {
		KeyData.Specs.Length = len;
	} else {
		$('#charCountModal').modal('show');
		len = KeyData.Specs.Length;
		if (!(!isNaN(len) && (len = parseInt(len)) <= 1024 && len >= 1))
			len = KeyData.Specs.Length = 1;

		document.getElementById("Len").value = len;
	}

	KeyData.Specs.LowCase = document.getElementById("LowCase").checked;
	KeyData.Specs.UpCase = document.getElementById("UpCase").checked;
	KeyData.Specs.Umlaut = document.getElementById("Umlaute").checked;
	KeyData.Specs.Numbers = document.getElementById("Numbers").checked;
	KeyData.Specs.Sym = document.getElementById("Symbols").checked;
	KeyData.Specs.Complex_Sym = document.getElementById("Com-Symbols").checked;
	KeyData.Specs.Very_Comp_Sym = document.getElementById("V-Com-Symbols").checked;
	KeyData.Specs.Custom = document.getElementById("Custom_Chars").checked;
	KeyData.Specs.CustomCharPool = document.getElementById("chars-custom").value;

	if ( //No Characters Available
		!KeyData.Specs.LowCase &&
		!KeyData.Specs.UpCase &&
		!KeyData.Specs.Umlaut &&
		!KeyData.Specs.Numbers &&
		!KeyData.Specs.Sym &&
		!KeyData.Specs.Complex_Sym &&
		!KeyData.Specs.Very_Comp_Sym &&
		!(KeyData.Specs.Custom && KeyData.Specs.CustomCharPool != "")) {

		// Do not show modal in following cases:
		if (!(sender != null && sender.id == "chars-custom" && !KeyData.Specs.Custom) &&
			!(sender != null && sender.id == "Custom_Chars" && KeyData.Specs.CustomCharPool == "") &&
			!(sender != null && sender.id == "Seed"))
			$('#noCharsModal').modal('show');
		var elements = document.getElementsByClassName("charpool");
		for (var i = 0; i < elements.length; i++) {
			var ele = elements[i];
			if (!ele.classList.contains("group-error")) {
				ele.classList.add("group-error");
			}
			document.getElementById("Pass").value = "";
		}
	} else {
		document.getElementById("Pass").value = KeyData.Password(document.getElementById("Seed").value);
		var elements = document.getElementsByClassName("charpool");
		for (var i = 0; i < elements.length; i++) {
			var ele = elements[i];
			if (ele.classList.contains("group-error")) {
				ele.classList.remove("group-error");
			}
		}
	}

}
/**
 * On Bootstrap input groups, allows
 * checkboxes to be toggled with their paired texts
 * @param {Node} sender the Checkbox
 */
function toggleCheck(sender) {
	/** @type {HTMLInputElement} */
	var checkbox = sender.parentNode.children[0].children[0];
	checkbox.checked = !checkbox.checked;
	updatePass(sender);
}

/**
 * Resets the form state to the stored/loaded values.
 */
function reset() {
	document.getElementById("LowCase").checked = KeyData.Specs.LowCase;
	document.getElementById("UpCase").checked = KeyData.Specs.UpCase;
	document.getElementById("Umlaute").checked = KeyData.Specs.Umlaut;
	document.getElementById("Numbers").checked = KeyData.Specs.Numbers;
	document.getElementById("Symbols").checked = KeyData.Specs.Sym;
	document.getElementById("Com-Symbols").checked = KeyData.Specs.Complex_Sym;
	document.getElementById("V-Com-Symbols").checked = KeyData.Specs.Very_Comp_Sym;
	document.getElementById("Custom_Chars").checked = KeyData.Specs.Custom;
	document.getElementById("Len").value = KeyData.Specs.Length;
	document.getElementById("chars-custom").value = KeyData.Specs.CustomCharPool;
	updatePass();
}

/**
 * Encrypts the password and submits the form.
 * If either or both of the password and username are invalid,
 * the action is cancelled
 */
function submitEncrypted() {
	if (validateInput()) {
		let enc_pass = document.getElementById("enc_pass");
		let unenc_pass = document.getElementById("unenc_pass");
		if (unenc_pass.value != "") {
			//Hide the form before changing it's content
			//to prevent the user from being confused...
			//enc_pass.parentElement.parentElement.hidden = true;
			enc_pass.value = sha256(unenc_pass.value);
			unenc_pass.value = "";
			if ('logIn' in document.forms) {
				document.forms['logIn'].submit();
			} else if ('register' in document.forms) {
				document.forms['register'].submit();
			}
		} else alert("Passwort fehlt. " +
			"Geben Sie ein Passwort ein. ");
	} else alert("Benutzername ist ungültig. " +
		"Benutzen Sie nur Zahlen, Buchstaben und die folgenden Zeichen: " +
		"'.', '-', '_', ' '");
}

/**
 * Returns true if the username is valid and false if not.
 */
function validateInput() {
	let validCharPool = "";
	validCharPool += charPools.LowCase;
	validCharPool += charPools.UpCase;
	validCharPool += charPools.Numbers;
	validCharPool += "._- ";
	let name = document.getElementsByName("name")[0].value;
	if (name != "") {
		for (var i = 0; i < name.length; i++) {
			if (validCharPool.indexOf(name.charAt(i)) == -1)
				return false;
		}
		return true;
	} else return false;
}

/**
 * An event handler to update the complexity bars for
 * passwords
 * @param {HTMLElement} sender The input which sent the event
 * @param {String} targetID The ID of the complexity bar to update
 */
function updateComplexity(sender, targetID) {
	/**  @type {String} */
	let pass = sender.value;
	let bar = document.getElementById(targetID);
	let passlen = pass.length;
	let numOfPools = countPools(pass);

	if (passlen == 0) setComplexity(bar, complexity.EMPTY);
	else if (passlen < 6 || numOfPools < 3) setComplexity(bar, complexity.LOW);
	else if (passlen < 9 || numOfPools < 4) setComplexity(bar, complexity.MEDIUM);
	else setComplexity(bar, complexity.HIGH);
}

/**
 * Counts the amount of characterpools in a string
 * @param {String} string The string to count for character pool containments.
 * @return {number} The amount of pools in the string
 */
function countPools(string) {
	let count = 0;
	if (containsPool(string, charPools.Complex_Sym)) count++;
	if (containsPool(string, charPools.LowCase)) count++;
	if (containsPool(string, charPools.Numbers)) count++;
	if (containsPool(string, charPools.Sym)) count++;
	if (containsPool(string, charPools.Umlaut)) count++;
	if (containsPool(string, charPools.UpCase)) count++;
	if (containsPool(string, charPools.Very_Comp_Sym)) count++;
	return count;
}

/**
 * Checks if the string contains any characters of the pool.
 * @param {String} string The string to check for the pool
 * @param {String} pool The pool to check in the string
 * @return {bool} True if a character of the pool is contained in string, otherwise false
 */
function containsPool(string, pool) {
	for (var i = 0; i < pool.length; i++) {
		if (string.indexOf(pool.charAt(i)) >= 0) return true;
	}
	return false;
}

/**
 * Sets the complexity bar to a specific state
 * @param {HTMLElement} target The progress bar
 * @param {number} value A property of the complexity enum
 */
function setComplexity(target, value) {
	var className = "progress-bar ";
	var width = 0;
	var content = "INVALID";
	switch (value) {
		default: case complexity.EMPTY:
			className += "progress-bar-info";
			width = 0;
			content = "Leer";
			break;
		case complexity.LOW:
			className += "progress-bar-danger";
			width = 33;
			content = "Unsicher";
			break;
		case complexity.MEDIUM:
			className += "progress-bar-warning";
			width = 67;
			content = "Durchschnittlich";
			break;
		case complexity.HIGH:
			className += "progress-bar-success";
			width = 100;
			content = "Sicher";
			break;
	}
	target.className = className;
	target.textContent = content;
	target.style.width = width + "%";
	target.setAttribute("aria-valuenow", width);
}

/**
 * Applies the master seed for the html-only version of the keygen.
 * @param {HTMLInputElement} sender 
 */
function setMasterHash(sender){
	var newSeed = sender.value;
	var result = "";

	if (newSeed.length == 0){
		Math.seedrandom(Date.now());
		newSeed = "" + Math.random();
	}
	for (var i = 0; i < newSeed.length; i++){
		result += String.fromCharCode(newSeed.charCodeAt(i) * (i+1));
	}
	KeyData.MasterHash = result;
	updatePass();
}